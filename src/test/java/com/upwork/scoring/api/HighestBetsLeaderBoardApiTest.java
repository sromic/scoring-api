package com.upwork.scoring.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.scoring.api.models.HighestBetsLeaderBoard;
import okhttp3.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class HighestBetsLeaderBoardApiTest {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static OkHttpClient client;

    static String HIGHEST_BETS_LEADER_BOARD_URL;

    static ObjectMapper objectMapper;

    @BeforeClass
    public static void prepare() {
        client = new OkHttpClient();
        HIGHEST_BETS_LEADER_BOARD_URL = "https://xfrmgczim0.execute-api.eu-west-1.amazonaws.com/dev/highestBetsLeaderboard";
        objectMapper = new ObjectMapper();
    }

    @Test
    public void getHighestBetsLeaderBoardPage1() {
        final int page = 1;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_BETS_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final Page<HighestBetsLeaderBoard> highestBetsLeaderBoardPage = objectMapper.readValue(response.body().string(), new TypeReference<PageImpl<HighestBetsLeaderBoard>>() {});
            assertNotNull(highestBetsLeaderBoardPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * requesting page number which exceeds total number of records in db
     * returned json must be: {
     "content": [],
     "last": true,
     "totalElements": 9,
     "totalPages": 1,
     "size": 100,
     "number": 9,
     "sort": null,
     "first": false,
     "numberOfElements": 0
     }
     */
    @Test
    public void getHighestBetsLeaderBoardPageToBig() {
        final int page = 1000000;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_BETS_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String highestBetsLeaderBoardPage = response.body().string();
            assertTrue(highestBetsLeaderBoardPage.contains("\"numberOfElements\":0"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getHighestBetsLeaderBoardInvalidPage() {
        final int page = -1;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_BETS_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String responseBody = response.body().string();
            final String PAGE_NUMBER_NOT_VALID = "\"page value not valid\"";
            assertEquals(PAGE_NUMBER_NOT_VALID, responseBody);
            assertEquals(response.code(), 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getHighestBetsLeaderBoardMissingPage() {
        final Request request = new Request.Builder()
                .url(HIGHEST_BETS_LEADER_BOARD_URL)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String responseBody = response.body().string();
            final String PAGE_NUMBER_IS_MISSING = "\"page is missing!\"";
            assertEquals(PAGE_NUMBER_IS_MISSING, responseBody);
            assertEquals(response.code(), 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
