package com.upwork.scoring.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HighestLevelLeaderBoardApiTest {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static OkHttpClient client;

    static String HIGHEST_LEVEL_LEADER_BOARD_URL;

    static ObjectMapper objectMapper;

    @BeforeClass
    public static void prepare() {
        client = new OkHttpClient();
        HIGHEST_LEVEL_LEADER_BOARD_URL = "https://xfrmgczim0.execute-api.eu-west-1.amazonaws.com/dev/highestLevelLeaderboard";
        objectMapper = new ObjectMapper();
    }

    /**
     * Returned json has data in `content` field, and `numberOfElements` is field with number of returned data objects
     * this should be bigger than 0
     */
    @Test
    public void getHighestLevelLeaderBoardPage1() {
        final int page = 1;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_LEVEL_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String highestBetsLeaderBoardPage = response.body().string();
            assertTrue(!highestBetsLeaderBoardPage.contains("\"numberOfElements\":0"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * requesting page number which exceeds total number of records in db
     * returned json must be: {
     "content": [],
     "last": true,
     "totalElements": 9,
     "totalPages": 1,
     "size": 100,
     "number": 9,
     "sort": null,
     "first": false,
     "numberOfElements": 0
     }
     */
    @Test
    public void getHighestLevelLeaderBoardPageToBig() {
        final int page = 1000000;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_LEVEL_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String highestBetsLeaderBoardPage = response.body().string();
            assertTrue(highestBetsLeaderBoardPage.contains("\"numberOfElements\":0"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getHighestLevelLeaderBoardInvalidPage() {
        final int page = -1;
        final String queryParamPage = "?page=";
        final Request request = new Request.Builder()
                .url(HIGHEST_LEVEL_LEADER_BOARD_URL + queryParamPage + page)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String responseBody = response.body().string();
            final String PAGE_NUMBER_NOT_VALID = "\"page value not valid\"";
            assertEquals(PAGE_NUMBER_NOT_VALID, responseBody);
            assertEquals(response.code(), 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getHighestLevelLeaderBoardMissingPage() {
        final Request request = new Request.Builder()
                .url(HIGHEST_LEVEL_LEADER_BOARD_URL)
                .build();

        final Response response;
        try {
            response = client.newCall(request).execute();
            final String responseBody = response.body().string();
            final String PAGE_NUMBER_IS_MISSING = "\"page is missing!\"";
            assertEquals(PAGE_NUMBER_IS_MISSING, responseBody);
            assertEquals(response.code(), 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
