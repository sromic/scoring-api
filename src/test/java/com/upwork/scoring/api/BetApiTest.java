package com.upwork.scoring.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.scoring.api.models.UserStats;
import com.upwork.scoring.api.requests.BetRequest;
import okhttp3.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BetApiTest {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static OkHttpClient client;

    static String BET_UPDATE_URL;

    static ObjectMapper objectMapper;

    @BeforeClass
    public static void prepare() {
        client = new OkHttpClient();
        BET_UPDATE_URL = "https://xfrmgczim0.execute-api.eu-west-1.amazonaws.com/dev/bet";
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testBetHandlerSuccess() {

        final BetRequest betRequest = new BetRequest();
        betRequest.setUserId(1L);
        betRequest.setWinnerEntryId(5L);
        betRequest.setWinnerNumOfLevels(100L);
        betRequest.setLosers(Arrays.asList(6L));

        try {
            final String betRequestAsJson = objectMapper.writeValueAsString(betRequest);

            final RequestBody body = RequestBody.create(JSON, betRequestAsJson);

            final Request request = new Request.Builder()
                    .url(BET_UPDATE_URL)
                    .put(body)
                    .build();

            final Response response = client.newCall(request).execute();

            final UserStats userStats = objectMapper.readValue(response.body().string(), UserStats.class);

            assertNotNull(userStats);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBetHandlerBadJson() {

        try {
            final String betRequestAsJson = "{\"winnerEntryId\":5,\"losers\":[6],\"winnerNumOfLevels\":100}";

            final RequestBody body = RequestBody.create(JSON, betRequestAsJson);
            final Request request = new Request.Builder()
                    .url(BET_UPDATE_URL)
                    .put(body)
                    .build();

            final Response response = client.newCall(request).execute();
            final String responseAsString = response.body().string();
            final String WRONG_OBJECT_TYPE = "\"Wrong object type!\"";

            assertEquals(WRONG_OBJECT_TYPE, responseAsString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
