package com.upwork.scoring.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.scoring.api.models.UserStatsExtended;
import okhttp3.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserStatsApiTest {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static OkHttpClient client;

    static String GET_USER_STATS_URL;

    static ObjectMapper objectMapper;

    @BeforeClass
    public static void prepare() {
        client = new OkHttpClient();
        GET_USER_STATS_URL = "https://xfrmgczim0.execute-api.eu-west-1.amazonaws.com/dev/userStats/";
        objectMapper = new ObjectMapper();
    }

    @Test
    public void getUserStatsSuccess() {
        final Long userId = 1L;

        final Request request = new Request.Builder()
                .url(GET_USER_STATS_URL+userId)
                .build();

        try {
            final Response response = client.newCall(request).execute();
            final UserStatsExtended userStatsExtended = objectMapper.readValue(response.body().string(), UserStatsExtended.class);

            assertNotNull(userStatsExtended);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserStatsMissingUserId() {
        final Request request = new Request.Builder()
                .url(GET_USER_STATS_URL)
                .build();

        try {
            final Response response = client.newCall(request).execute();
            final String responseBody = response.body().string();

            final String MISSING_AUTH_TOKEN_JSON = "{\"message\":\"Missing Authentication Token\"}";

            assertEquals(responseBody, MISSING_AUTH_TOKEN_JSON);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserStatsUnknownUserId() {
        final Long userId = 10000000L;
        final Request request = new Request.Builder()
                .url(GET_USER_STATS_URL+userId)
                .build();

        try {
            final Response response = client.newCall(request).execute();
            final String responseBody = response.body().string();

            //empty response if userId does not exists in db
            assertEquals(responseBody, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 }
