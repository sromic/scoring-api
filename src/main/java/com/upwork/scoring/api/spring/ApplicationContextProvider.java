package com.upwork.scoring.api.spring;

import org.springframework.context.ApplicationContext;

/**
 * Interface for holding Spring application context
 */
public interface ApplicationContextProvider {
    ApplicationContext getApplicationContext();
}
