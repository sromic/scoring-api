package com.upwork.scoring.api.repositories;

import com.upwork.scoring.api.models.Entry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
@Transactional
public interface EntryRepository extends PagingAndSortingRepository<Entry, Long> {
    @Query(value = "SELECT e.* FROM User u INNER JOIN Bet b ON u.UserId = b.UserId INNER JOIN Entry e ON b.EntryId = e.EntryId WHERE u.UserId = ?1 AND e.EntryId IN ?2", nativeQuery = true)
    Set<Entry> findEntriesByUserIdAndEntriesIds(Long userId, List<Long> entriesId);
}
