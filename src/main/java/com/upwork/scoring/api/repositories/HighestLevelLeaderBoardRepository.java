package com.upwork.scoring.api.repositories;
import com.upwork.scoring.api.models.HighestLevelLeaderBoard;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface HighestLevelLeaderBoardRepository extends PagingAndSortingRepository<HighestLevelLeaderBoard,Long>{
}