package com.upwork.scoring.api.repositories;

import com.upwork.scoring.api.models.UserStats;
import com.upwork.scoring.api.models.UserStatsExtended;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserStatsRepository extends PagingAndSortingRepository<UserStats, Long> {

    @Modifying
    @Query(value = "insert into scoring.UserStats\n" +
            "(\n" +
            "  UserId,\n" +
            "  NumOfActiveBets,\n" +
            "  TotalNumOfBets,\n" +
            "  HighestLevel,\n" +
            "  HighestLevelEntryId\n" +
            ")\n" +
            "  SELECT *\n" +
            "  FROM(\n" +
            "        select sq_all.UserId,\n" +
            "          sq_all.ActiveEntries,\n" +
            "          sq_all.NumOfEntries,\n" +
            "          sq_max.maxLevel,\n" +
            "          sq_max.maxEntryId\n" +
            "        FROM(\n" +
            "              select  u.UserId,\n" +
            "                SUM(CASE WHEN e.IsActive=1 then 1 else 0 end) ActiveEntries,\n" +
            "                count(*) NumOfEntries\n" +
            "              from scoring.User u\n" +
            "                left join scoring.Bet b\n" +
            "                  on (u.UserId = b.UserId)\n" +
            "                join scoring.Entry e\n" +
            "                  on (b.EntryId = e.EntryId)\n" +
            "              where b.UserId = ?1) sq_all\n" +
            "          INNER JOIN\n" +
            "          (\n" +
            "            select b.UserId,\n" +
            "              max(e.EntryId) maxEntryId,\n" +
            "              max(NumOfLevels) maxLevel\n" +
            "            from  scoring.Bet b\n" +
            "              inner join scoring.Entry e\n" +
            "                on (b.EntryId = e.EntryId)\n" +
            "            where b.UserId = ?1 and(b.UserId, NumOfLevels) in(\n" +
            "              select b.UserId,max(NumOfLevels)\n" +
            "              from  scoring.Bet b\n" +
            "                inner join scoring.Entry e\n" +
            "                  on (b.EntryId = e.EntryId)\n" +
            "              where b.UserId = ?1\n" +
            "            )\n" +
            "          ) sq_max\n" +
            "            ON (sq_max.UserId = sq_all.UserId)) dada\n" +
            "ON duplicate key\n" +
            "UPDATE  NumOfActiveBets = dada.ActiveEntries,\n" +
            "  TotalNumOfBets = dada.NumOfEntries,\n" +
            "  HighestLevel = dada.maxLevel,\n" +
            "  HighestLevelEntryId = dada.maxEntryId", nativeQuery = true)
    void recalculateUserStatsForUser(Long userId);

    @Query(nativeQuery =true)
    UserStatsExtended findUserStatsByUserId(Long userId);
}
