package com.upwork.scoring.api.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.upwork.scoring.api.constants.HttpStatusCode;
import com.upwork.scoring.api.factories.PageRequestFactory;
import com.upwork.scoring.api.models.HighestLevelLeaderBoard;
import com.upwork.scoring.api.repositories.HighestLevelLeaderBoardRepository;
import com.upwork.scoring.api.responses.ApiGatewayResponse;
import com.upwork.scoring.api.validators.HighestLevelLeaderBoardValidator;
import com.upwork.scoring.api.validators.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class HighestLevelLeaderBoardHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private static final Logger LOG = Logger.getLogger(HighestBetsLeaderBoardHandler.class);

    private final HighestLevelLeaderBoardRepository highestLevelLeaderBoardRepository;

    private final HighestLevelLeaderBoardValidator highestLevelLeaderBoardValidator;

    @Autowired
    public HighestLevelLeaderBoardHandler(HighestLevelLeaderBoardRepository highestLevelLeaderBoardRepository,
                                          HighestLevelLeaderBoardValidator highestLevelLeaderBoardValidatorImpl) {
        this.highestLevelLeaderBoardRepository = highestLevelLeaderBoardRepository;
        this.highestLevelLeaderBoardValidator = highestLevelLeaderBoardValidatorImpl;
    }
    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {

        final String validationResults = highestLevelLeaderBoardValidator.validate(input);

        if (Validator.NO_ERRORS == validationResults) {
            final Map<String, Object> queryStringParameters = (Map<String, Object>) input.get("queryStringParameters");

            final Integer page = Integer.valueOf((String) queryStringParameters.get("page"));

            LOG.info("Handling page: " + page);

            //get paginate data from given page with offset of 100
            final PageRequest pageRequest = PageRequestFactory.getPageRequest(page - 1, Sort.Direction.ASC, new String[]{HighestLevelLeaderBoard.TableKeys.POSITION});
            final Page<HighestLevelLeaderBoard> highestLevelLederboardsPage = highestLevelLeaderBoardRepository.findAll(pageRequest);

            return ApiGatewayResponse.builder()
                    .setStatusCode(HttpStatusCode.OK.getValue())
                    .setObjectBody(highestLevelLederboardsPage)
                    .build();
        }

        return ApiGatewayResponse.builder()
                .setStatusCode(HttpStatusCode.BAD_REQUEST.getValue())
                .setObjectBody(validationResults)
                .build();
    }
}