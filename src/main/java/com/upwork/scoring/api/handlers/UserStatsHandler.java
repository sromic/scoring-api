package com.upwork.scoring.api.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.upwork.scoring.api.constants.HttpStatusCode;
import com.upwork.scoring.api.models.UserStatsExtended;
import com.upwork.scoring.api.repositories.UserStatsRepository;
import com.upwork.scoring.api.responses.ApiGatewayResponse;
import com.upwork.scoring.api.validators.UserStatsHandlerValidator;
import com.upwork.scoring.api.validators.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class UserStatsHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private static final Logger LOG = Logger.getLogger(BetHandler.class);

    private final UserStatsRepository userStatsRepository;

    private final UserStatsHandlerValidator userStatsHandlerValidator;

    @Autowired
    public UserStatsHandler(UserStatsRepository userStatsRepository, UserStatsHandlerValidator userStatsHandlerValidator) {
        this.userStatsRepository = userStatsRepository;
        this.userStatsHandlerValidator = userStatsHandlerValidator;
    }

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {

        final String validationResults = userStatsHandlerValidator.validate(input);

        if (Validator.NO_ERRORS == validationResults) {
            final Map<String, Object> pathParameters = (Map<String, Object>) input.get("pathParameters");
            final Long userId = Long.valueOf((String) pathParameters.get("userId"));

            LOG.info("Handling userId: " + userId);

            //get data from user stats for given userId and
            //join with positions from HighestLevelLeaderBoard and HighestBetsLeaderBoard tables
            final UserStatsExtended userStatsForUserId = userStatsRepository.findUserStatsByUserId(userId);

            return ApiGatewayResponse.builder()
                    .setStatusCode(HttpStatusCode.OK.getValue())
                    .setObjectBody(userStatsForUserId)
                    .build();
        }

        return ApiGatewayResponse.builder()
                    .setStatusCode(HttpStatusCode.BAD_REQUEST.getValue())
                    .setObjectBody(validationResults)
                    .build();
    }
}
