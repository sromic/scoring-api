package com.upwork.scoring.api.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.scoring.api.constants.HttpStatusCode;
import com.upwork.scoring.api.models.Entry;
import com.upwork.scoring.api.models.UserStats;
import com.upwork.scoring.api.repositories.EntryRepository;
import com.upwork.scoring.api.repositories.UserStatsRepository;
import com.upwork.scoring.api.requests.BetRequest;
import com.upwork.scoring.api.responses.ApiGatewayResponse;
import com.upwork.scoring.api.validators.BetHandlerValidator;
import com.upwork.scoring.api.validators.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class BetHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private static final Logger LOG = Logger.getLogger(BetHandler.class);

    private final EntryRepository entryRepository;

    private final UserStatsRepository userStatsRepository;

    private final BetHandlerValidator betHandlerValidator;

    @Autowired
    public BetHandler(EntryRepository entryRepository, UserStatsRepository userStatsRepository, BetHandlerValidator betHandlerValidator) {
        this.entryRepository = entryRepository;
        this.userStatsRepository = userStatsRepository;
        this.betHandlerValidator = betHandlerValidator;
    }

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {

        final String validationResults = betHandlerValidator.validate(input);

        if (Validator.NO_ERRORS == validationResults) {
            final String body = (String) input.get("body");
            UserStats recalculatedUserStats = null;

            try {
                final ObjectMapper objectMapper = new ObjectMapper();
                //deserialize json to BetRequest instance
                final BetRequest betRequest = objectMapper.readValue(body, BetRequest.class);

                final Long userId = betRequest.getUserId();
                final Long winnerEntryId = betRequest.getWinnerEntryId();
                final List<Long> winnerEntryList = Arrays.asList(winnerEntryId);

                final Set<Entry> winnerEntries = entryRepository.findEntriesByUserIdAndEntriesIds(userId, winnerEntryList);
                final Set<Entry> loserEntries = entryRepository.findEntriesByUserIdAndEntriesIds(userId, betRequest.getLosers());

                winnerEntries.stream().forEach(entry -> entry.setNumOfLevels(betRequest.getWinnerNumOfLevels()));

                loserEntries.stream().forEach(entry -> entry.setIsActive((short) 0));

                //merge winner and loser entries
                final Set<Entry> allBets = Stream.concat(winnerEntries.stream(), loserEntries.stream()).collect(Collectors.toSet());

                //update them
                entryRepository.save(allBets);

                //recalculate user stats for given user
                userStatsRepository.recalculateUserStatsForUser(userId);

                //get user stats for given userId
                recalculatedUserStats = userStatsRepository.findOne(userId);
            } catch (IOException e) {
                LOG.error("Error while parsing request: " + e.getMessage());
            }

            return ApiGatewayResponse.builder()
                    .setStatusCode(HttpStatusCode.OK.getValue())
                    .setObjectBody(recalculatedUserStats)
                    .build();
        }

        return ApiGatewayResponse.builder()
                .setStatusCode(HttpStatusCode.BAD_REQUEST.getValue())
                .setObjectBody(validationResults)
                .build();
    }
}
