package com.upwork.scoring.api.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.upwork.scoring.api.constants.HttpStatusCode;
import com.upwork.scoring.api.factories.PageRequestFactory;
import com.upwork.scoring.api.models.HighestBetsLeaderBoard;
import com.upwork.scoring.api.repositories.HighestBetsLeaderBoardRepository;
import com.upwork.scoring.api.responses.ApiGatewayResponse;
import com.upwork.scoring.api.validators.HighestBetsLeaderBoardHandlerValidator;
import com.upwork.scoring.api.validators.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class HighestBetsLeaderBoardHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private static final Logger LOG = Logger.getLogger(HighestBetsLeaderBoardHandler.class);

    private final HighestBetsLeaderBoardRepository highestBetsLeaderBoardRepository;

    private final HighestBetsLeaderBoardHandlerValidator highestBetsLeaderBoardHandlerValidator;

        @Autowired
        public HighestBetsLeaderBoardHandler(HighestBetsLeaderBoardRepository highestBetsLeaderBoardRepository,
                                             HighestBetsLeaderBoardHandlerValidator highestBetsLeaderBoardHandlerValidator) {
            this.highestBetsLeaderBoardRepository = highestBetsLeaderBoardRepository;
            this.highestBetsLeaderBoardHandlerValidator = highestBetsLeaderBoardHandlerValidator;
        }
        @Override
        public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {

            final String validationResults = highestBetsLeaderBoardHandlerValidator.validate(input);

            if (Validator.NO_ERRORS == validationResults) {
                final Map<String, Object> queryStringParameters = (Map<String, Object>) input.get("queryStringParameters");

                final Integer page = Integer.valueOf((String) queryStringParameters.get("page"));

                LOG.info("Handling page: " + page);

                //get paginate data from given page with offset of 100
                final PageRequest pageRequest = PageRequestFactory.getPageRequest(page - 1, Sort.Direction.ASC, new String[]{HighestBetsLeaderBoard.TableKeys.POSITION});
                final Page<HighestBetsLeaderBoard> highBLBoard = highestBetsLeaderBoardRepository.findAll(pageRequest);

                return ApiGatewayResponse.builder()
                        .setStatusCode(HttpStatusCode.OK.getValue())
                        .setObjectBody(highBLBoard)
                        .build();
            }

            return ApiGatewayResponse.builder()
                    .setStatusCode(HttpStatusCode.BAD_REQUEST.getValue())
                    .setObjectBody(validationResults)
                    .build();
        }
}