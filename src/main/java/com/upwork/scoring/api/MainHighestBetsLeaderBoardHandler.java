package com.upwork.scoring.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.upwork.scoring.api.handlers.HighestBetsLeaderBoardHandler;
import com.upwork.scoring.api.responses.ApiGatewayResponse;
import com.upwork.scoring.api.spring.SpringRequestHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

/**
 * This class is entry point to HighestBetsLeaderboard handler
 */
public class MainHighestBetsLeaderBoardHandler extends SpringRequestHandler<Map<String, Object>, ApiGatewayResponse> {

    /**
     * initialise specific handler for this Lambda function
     */
    public MainHighestBetsLeaderBoardHandler(){
        this.handler = this.context.getBean(HighestBetsLeaderBoardHandler.class);
    }

    private static final ApplicationContext context =
            new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

    @Override
    public ApplicationContext getApplicationContext() {
        return context;
    }

    /**
     * Invoke request with defined requestHandler
     * @param input
     * @param context
     * @return
     */
    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        return handler.handleRequest(input, context);
    }

    @Override
    public RequestHandler getHandler() {
        return super.getHandler();
    }

    @Override
    public void setHandler(RequestHandler handler) {
        super.setHandler(handler);
    }
}