package com.upwork.scoring.api.credentials;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;

/**
 * Using this store as double checked singleton with environment variable holding required keys
 *
 * if you want to use credentials file then create file /Users/userid/.aws/credentials with content like :
 * [profileName]
 aws_access_key_id=XXXXX
 aws_secret_access_key=YYYYYY

 * and instead of {@link EnvironmentVariableCredentialsProvider} use {@link ProfileCredentialsProvider}
 */
public final class CredentialsStore {
    private static volatile AWSCredentialsProvider credentialsProvider;

    public static AWSCredentialsProvider getCredentialsProvider() {
        if (credentialsProvider == null) {
            synchronized (CredentialsStore.class) {
                if (credentialsProvider == null) {
                    credentialsProvider = new EnvironmentVariableCredentialsProvider();
                }
            }
        }

        return credentialsProvider;
    }
}

