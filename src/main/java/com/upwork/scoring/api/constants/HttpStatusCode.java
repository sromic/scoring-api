package com.upwork.scoring.api.constants;

public enum HttpStatusCode {
    OK(200), BAD_REQUEST(400);

    private HttpStatusCode(int value) {
        this.value = value;
    }

    private final int value;

    public int getValue() {
        return value;
    }
}
