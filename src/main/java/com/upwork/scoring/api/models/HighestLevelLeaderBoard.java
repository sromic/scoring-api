package com.upwork.scoring.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "HighestLevelLeaderBoard")
public class HighestLevelLeaderBoard {

    @Id
    @Column(name = "UserId")
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "NumberOfLevels")
    private Long totalNumOfLevels;

    @Column(name = "Position")
    private Long position;

    public HighestLevelLeaderBoard(){}

    public HighestLevelLeaderBoard(Long position, Long id, String name, Long totalNumOfLevels) {
        this.position=position;
        this.id=id;
        this.name=name;
        this.totalNumOfLevels=totalNumOfLevels;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTotalNumOfLevels() {
        return totalNumOfLevels;
    }

    public void setTotalNumOfLevels(Long totalNumOfLevels) {
        this.totalNumOfLevels = totalNumOfLevels;
    }

    public interface TableKeys {
        String POSITION = "position";

        String USER_ID = "id";

        String NAME = "name";

        String TOTAL_NUMBER_OF_LEVELS = "totalNumOfLevels";
    }

}
