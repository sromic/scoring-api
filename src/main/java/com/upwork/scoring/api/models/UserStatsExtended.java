package com.upwork.scoring.api.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserStatsExtended {
    private final Long userId;

    private final Long numOfActiveBets;

    private final Long totalNumOfBets;

    private final Long highestLevel;

    private final Long highestLevelEntryId;

    private final Long highestLevelPosition;

    private final Long totalVotesPosition;

    @JsonCreator
    public UserStatsExtended(@JsonProperty("userId") Long userId,
                             @JsonProperty("numOfActiveBets") Long numOfActiveBets,
                             @JsonProperty("totalNumOfBets") Long totalNumOfBets,
                             @JsonProperty("highestLevel") Long highestLevel,
                             @JsonProperty("highestLevelEntryId") Long highestLevelEntryId,
                             @JsonProperty("highestLevelPosition") Long highestLevelPosition,
                             @JsonProperty("totalVotesPosition") Long totalVotesPosition) {
        this.userId = userId;
        this.numOfActiveBets = numOfActiveBets;
        this.totalNumOfBets = totalNumOfBets;
        this.highestLevel = highestLevel;
        this.highestLevelEntryId = highestLevelEntryId;
        this.highestLevelPosition = highestLevelPosition;
        this.totalVotesPosition = totalVotesPosition;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getNumOfActiveBets() {
        return numOfActiveBets;
    }

    public Long getTotalNumOfBets() {
        return totalNumOfBets;
    }

    public Long getHighestLevel() {
        return highestLevel;
    }

    public Long getHighestLevelEntryId() {
        return highestLevelEntryId;
    }

    public Long getHighestLevelPosition() {
        return highestLevelPosition;
    }

    public Long getTotalVotesPosition() {
        return totalVotesPosition;
    }

    @Override
    public String toString() {
        return "UserStatsExtended{" +
                "userId=" + userId +
                ", numOfActiveBets=" + numOfActiveBets +
                ", totalNumOfBets=" + totalNumOfBets +
                ", highestLevel=" + highestLevel +
                ", highestLevelEntryId=" + highestLevelEntryId +
                ", highestLevelPosition=" + highestLevelPosition +
                ", totalVotesPosition=" + totalVotesPosition +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserStatsExtended that = (UserStatsExtended) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (numOfActiveBets != null ? !numOfActiveBets.equals(that.numOfActiveBets) : that.numOfActiveBets != null)
            return false;
        if (totalNumOfBets != null ? !totalNumOfBets.equals(that.totalNumOfBets) : that.totalNumOfBets != null)
            return false;
        if (highestLevel != null ? !highestLevel.equals(that.highestLevel) : that.highestLevel != null) return false;
        if (highestLevelEntryId != null ? !highestLevelEntryId.equals(that.highestLevelEntryId) : that.highestLevelEntryId != null)
            return false;
        if (highestLevelPosition != null ? !highestLevelPosition.equals(that.highestLevelPosition) : that.highestLevelPosition != null)
            return false;
        return totalVotesPosition != null ? totalVotesPosition.equals(that.totalVotesPosition) : that.totalVotesPosition == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (numOfActiveBets != null ? numOfActiveBets.hashCode() : 0);
        result = 31 * result + (totalNumOfBets != null ? totalNumOfBets.hashCode() : 0);
        result = 31 * result + (highestLevel != null ? highestLevel.hashCode() : 0);
        result = 31 * result + (highestLevelEntryId != null ? highestLevelEntryId.hashCode() : 0);
        result = 31 * result + (highestLevelPosition != null ? highestLevelPosition.hashCode() : 0);
        result = 31 * result + (totalVotesPosition != null ? totalVotesPosition.hashCode() : 0);
        return result;
    }
}
