package com.upwork.scoring.api.models;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "Entry")
@Entity
public class Entry implements Serializable {

    @Id
    @Column(name = "EntryId")
    private Long id;

    @Column(name = "CreatorUserId", nullable = false)
    private Long creatorUserId;

    @Column(name = "NumOfLevels")
    private Long numOfLevels;

    @Column(name = "IsActive")
    private Short isActive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Long getNumOfLevels() {
        return numOfLevels;
    }

    public void setNumOfLevels(Long numOfLevels) {
        this.numOfLevels = numOfLevels;
    }

    public Short getIsActive() {
        return isActive;
    }

    public void setIsActive(Short isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", creatorUserId=" + creatorUserId +
                ", numOfLevels=" + numOfLevels +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entry entry = (Entry) o;

        if (id != null ? !id.equals(entry.id) : entry.id != null) return false;
        if (creatorUserId != null ? !creatorUserId.equals(entry.creatorUserId) : entry.creatorUserId != null)
            return false;
        if (numOfLevels != null ? !numOfLevels.equals(entry.numOfLevels) : entry.numOfLevels != null) return false;
        return isActive != null ? isActive.equals(entry.isActive) : entry.isActive == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (creatorUserId != null ? creatorUserId.hashCode() : 0);
        result = 31 * result + (numOfLevels != null ? numOfLevels.hashCode() : 0);
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        return result;
    }
}
