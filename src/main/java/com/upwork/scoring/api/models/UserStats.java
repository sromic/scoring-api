package com.upwork.scoring.api.models;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "UserStats")
@Entity
@SqlResultSetMapping(
        name="userStatsMapping",
        classes={
                @ConstructorResult(
                        targetClass=com.upwork.scoring.api.models.UserStatsExtended.class,
                        columns={
                                @ColumnResult(name="UserId", type = Long.class),
                                @ColumnResult(name="NumOfActiveBets", type = Long.class),
                                @ColumnResult(name="TotalNumOfBets", type = Long.class),
                                @ColumnResult(name="HighestLevel", type = Long.class),
                                @ColumnResult(name="HighestLevelEntryId", type = Long.class),
                                @ColumnResult(name="HighestLevelPosition", type = Long.class),
                                @ColumnResult(name="TotalVotesPosition", type = Long.class)
                        }
                )
        }
)
@NamedNativeQuery(name="UserStats.findUserStatsByUserId", query="SELECT us.*, HighestLevelLeaderBoard.Position as HighestLevelPosition, HighestBetsLeaderboard.Position as TotalVotesPosition\n" +
        "FROM UserStats us\n" +
        "INNER JOIN HighestLevelLeaderBoard ON us.UserId = HighestLevelLeaderBoard.UserId\n" +
        "INNER JOIN HighestBetsLeaderboard ON us.UserId = us.UserId = HighestBetsLeaderboard.UserId\n" +
        "WHERE us.UserId = ?1", resultSetMapping="userStatsMapping")
public class UserStats implements Serializable {

    @Id
    @Column(name = "UserId", nullable = false)
    private Long userId;

    @Column(name = "NumOfActiveBets")
    private Long numOfActiveBets;

    @Column(name = "TotalNumOfBets")
    private Long totalNumOfBets;

    @Column(name = "HighestLevel")
    private Long highestLevel;

    @Column(name = "HighestLevelEntryId")
    private Long highestLevelEntryId;

    public UserStats(){}

    public UserStats(Long userId, Long numOfActiveBets, Long totalNumOfBets, Long highestLevel, Long highestLevelEntryId) {
        this.userId = userId;
        this.numOfActiveBets = numOfActiveBets;
        this.totalNumOfBets = totalNumOfBets;
        this.highestLevel = highestLevel;
        this.highestLevelEntryId = highestLevelEntryId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNumOfActiveBets() {
        return numOfActiveBets;
    }

    public void setNumOfActiveBets(Long numOfActiveBets) {
        this.numOfActiveBets = numOfActiveBets;
    }

    public Long getTotalNumOfBets() {
        return totalNumOfBets;
    }

    public void setTotalNumOfBets(Long totalNumOfBets) {
        this.totalNumOfBets = totalNumOfBets;
    }

    public Long getHighestLevel() {
        return highestLevel;
    }

    public void setHighestLevel(Long highestLevel) {
        this.highestLevel = highestLevel;
    }

    public Long getHighestLevelEntryId() {
        return highestLevelEntryId;
    }

    public void setHighestLevelEntryId(Long highestLevelEntryId) {
        this.highestLevelEntryId = highestLevelEntryId;
    }

    @Override
    public String toString() {
        return "UserStats{" +
                "userId=" + userId +
                ", numOfActiveBets=" + numOfActiveBets +
                ", totalNumOfBets=" + totalNumOfBets +
                ", highestLevel=" + highestLevel +
                ", highestLevelEntryId=" + highestLevelEntryId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserStats userStats = (UserStats) o;

        if (userId != null ? !userId.equals(userStats.userId) : userStats.userId != null) return false;
        if (numOfActiveBets != null ? !numOfActiveBets.equals(userStats.numOfActiveBets) : userStats.numOfActiveBets != null)
            return false;
        if (totalNumOfBets != null ? !totalNumOfBets.equals(userStats.totalNumOfBets) : userStats.totalNumOfBets != null)
            return false;
        if (highestLevel != null ? !highestLevel.equals(userStats.highestLevel) : userStats.highestLevel != null)
            return false;
        return highestLevelEntryId != null ? highestLevelEntryId.equals(userStats.highestLevelEntryId) : userStats.highestLevelEntryId == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (numOfActiveBets != null ? numOfActiveBets.hashCode() : 0);
        result = 31 * result + (totalNumOfBets != null ? totalNumOfBets.hashCode() : 0);
        result = 31 * result + (highestLevel != null ? highestLevel.hashCode() : 0);
        result = 31 * result + (highestLevelEntryId != null ? highestLevelEntryId.hashCode() : 0);
        return result;
    }
}
