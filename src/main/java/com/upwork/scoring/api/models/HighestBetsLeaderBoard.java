package com.upwork.scoring.api.models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "HighestBetsLeaderBoard")
public class HighestBetsLeaderBoard implements Serializable {
    @Id
    @Column(name = "UserId")
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Position")
    private Long position;

    @Column(name = "TotalNumOfBets")
    private Long totalnumofbets;

    public HighestBetsLeaderBoard(){}

    public HighestBetsLeaderBoard(Long id, String name, Long position, Long totalnumofbets) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.totalnumofbets = totalnumofbets;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPosition() {
        return position;
    }

    public Long getTotalnumofbets() {
        return totalnumofbets;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public void setTotalnumofbets(Long totalnumofbets) {
        this.totalnumofbets = totalnumofbets;
    }

    @Override
    public String toString() {
        return "HighestBetsLeaderBoard{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position=" + position +
                ", totalnumofbets=" + totalnumofbets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HighestBetsLeaderBoard)) return false;

        HighestBetsLeaderBoard that = (HighestBetsLeaderBoard) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (position != null ? !position.equals(that.position) : that.position != null) return false;
        return totalnumofbets != null ? totalnumofbets.equals(that.totalnumofbets) : that.totalnumofbets == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (totalnumofbets != null ? totalnumofbets.hashCode() : 0);
        return result;
    }

    public interface TableKeys {
        String POSITION = "position";

        String USER_ID = "id";

        String NAME = "name";

        String TOTAL_NUMBER_OF_BETS = "totalnumofbets";
    }
}
