package com.upwork.scoring.api.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Represents PUT bet request
 */
public class BetRequest implements Serializable {

    public BetRequest(){}

    @JsonCreator
    public BetRequest(@JsonProperty(value = "userId", required = true) Long userId,
                      @JsonProperty(value = "winnerEntryId", required = true) Long winnerEntryId,
                      @JsonProperty(value = "losers") List<Long> losers,
                      @JsonProperty(value = "winnerNumOfLevels", required = true) Long winnerNumOfLevels) {
        this.userId = userId;
        this.winnerEntryId = winnerEntryId;
        this.losers = losers;
        this.winnerNumOfLevels = winnerNumOfLevels;
    }

    @JsonProperty(required = true)
    private Long userId;

    @JsonProperty(required = true)
    private Long winnerEntryId;

    @JsonProperty(required = true)
    private List<Long> losers;

    @JsonProperty(required = true)
    private long winnerNumOfLevels;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWinnerEntryId() {
        return winnerEntryId;
    }

    public void setWinnerEntryId(Long winnerEntryId) {
        this.winnerEntryId = winnerEntryId;
    }

    public List<Long> getLosers() {
        return losers;
    }

    public void setLosers(List<Long> losers) {
        this.losers = losers;
    }

    public long getWinnerNumOfLevels() {
        return winnerNumOfLevels;
    }

    public void setWinnerNumOfLevels(long winnerNumOfLevels) {
        this.winnerNumOfLevels = winnerNumOfLevels;
    }

    @Override
    public String toString() {
        return "BetRequest{" +
                "userId='" + userId + '\'' +
                ", winnerEntryId='" + winnerEntryId + '\'' +
                ", losers=" + losers +
                ", winnerNumOfLevels=" + winnerNumOfLevels +
                '}';
    }

}
