package com.upwork.scoring.api.factories;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Abstract factory class for generating PageRequest instance
 */
public final class PageRequestFactory {

    private static final int MAX_RESULTS_PER_PAGE = 100;

    public static PageRequest getPageRequest(int page, Sort.Direction direction, String... properties) {
        final PageRequest request = new PageRequest(page, MAX_RESULTS_PER_PAGE, direction, properties);

        return request;
    }
}
