package com.upwork.scoring.api.validators.impl;

import com.upwork.scoring.api.validators.UserStatsHandlerValidator;
import com.upwork.scoring.api.validators.Validator;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserStatsHandlerValidatorImpl implements UserStatsHandlerValidator {
    @Override
    public String validate(Map<String, Object> input) {
        final Map<String, Object> pathParameters = (Map<String, Object>) input.get("pathParameters");

        if (null == pathParameters || (null != pathParameters && null == pathParameters.get("userId")))
            return "userId as path parameter is missing!";

        return Validator.NO_ERRORS;
    }
}
