package com.upwork.scoring.api.validators;

import java.util.Map;

public interface UserStatsHandlerValidator extends Validator<Map<String, Object>, String> {
}
