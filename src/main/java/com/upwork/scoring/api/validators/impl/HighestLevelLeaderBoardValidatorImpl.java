package com.upwork.scoring.api.validators.impl;

import com.amazonaws.util.StringUtils;
import com.upwork.scoring.api.validators.HighestLevelLeaderBoardValidator;
import com.upwork.scoring.api.validators.Validator;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class HighestLevelLeaderBoardValidatorImpl implements HighestLevelLeaderBoardValidator {
    @Override
    public String validate(Map<String, Object> input) {
        final Map<String, Object> queryParameters = (Map<String, Object>) input.get("queryStringParameters");

        if (null == queryParameters || null == queryParameters.get("page"))
            return "page is missing!";
        else if (StringUtils.isNullOrEmpty((String) queryParameters.get("page")) || Integer.valueOf((String) queryParameters.get("page")) <= 0)
                return "page value not valid";

        return Validator.NO_ERRORS;
    }
}