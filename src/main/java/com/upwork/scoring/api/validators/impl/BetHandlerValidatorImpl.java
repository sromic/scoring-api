package com.upwork.scoring.api.validators.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.scoring.api.requests.BetRequest;
import com.upwork.scoring.api.validators.BetHandlerValidator;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.Map;

@Component
public class BetHandlerValidatorImpl implements BetHandlerValidator {

    @Override
    public String validate(Map<String, Object> input) {
        final String body = (String) input.get("body");
        if (null == body)
            return "Missing body payload";

        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            final BetRequest betRequest = objectMapper.readValue(body, BetRequest.class);
        } catch (IOException e) {
            return "Wrong object type!";
        }

        return NO_ERRORS;
    }
}
