package com.upwork.scoring.api.validators;

public interface Validator<O, R> {
    String NO_ERRORS = "";

    R validate(O input);
}
