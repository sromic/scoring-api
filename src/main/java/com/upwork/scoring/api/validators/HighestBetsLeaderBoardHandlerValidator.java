package com.upwork.scoring.api.validators;

import java.util.Map;

public interface HighestBetsLeaderBoardHandlerValidator extends Validator<Map<String,Object>,String> {
}
