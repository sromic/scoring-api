package com.upwork.scoring.api.validators;

import java.util.Map;

public interface BetHandlerValidator extends Validator<Map<String, Object>, String> {}
