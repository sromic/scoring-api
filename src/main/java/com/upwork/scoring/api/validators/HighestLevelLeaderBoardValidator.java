package com.upwork.scoring.api.validators;

import java.util.Map;

public interface HighestLevelLeaderBoardValidator extends Validator<Map<String, Object>, String> {
}
